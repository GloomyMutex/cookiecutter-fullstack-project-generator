import os
import secrets
from typing import List, Union

from pydantic import AnyHttpUrl, BaseSettings, EmailStr, HttpUrl, PostgresDsn, validator


class Settings(BaseSettings):
    API_V1_STR: str = "/api/v1"

    SECRET_KEY: str = secrets.token_urlsafe(32)

    # 60 minutes * 24 hours * 8 days = 8 days
    ACCESS_TOKEN_EXPIRE_MINUTES: int = 60 * 24 * 8  

    SERVER_NAME: str = None
    SERVER_HOST: Union[str, HttpUrl] = None
    # BACKEND_CORS_ORIGINS is a string of origins separated by commas, e.g: "http://localhost, http://localhost:4200, http://localhost:3000, http://localhost:8080, http://local.dockertoolbox.tiangolo.com"
    BACKEND_CORS_ORIGINS: Union[str, List[AnyHttpUrl]] = None

    @validator("BACKEND_CORS_ORIGINS", pre=True)
    def assemble_cors_origins(cls, v):
        if isinstance(v, str):
            return [i.strip() for i in v.split(",")]
        return v

    PROJECT_NAME: str = None
    SENTRY_DSN: Union[str, HttpUrl] = None

    POSTGRES_SERVER: str = None
    POSTGRES_USER: str = None
    POSTGRES_PASSWORD: str = None
    POSTGRES_DB: str = None
    SQLALCHEMY_DATABASE_URI: PostgresDsn = None

    @validator("SQLALCHEMY_DATABASE_URI")
    def assemble_db_connection(cls, v, values):
        return "postgresql://{POSTGRES_USER}:{POSTGRES_PASSWORD}@{POSTGRES_SERVER}/{POSTGRES_DB}".format(
            **values
        )
    
    SMTP_TLS: bool = True
    SMTP_PORT: int = None
    SMTP_HOST: str = None
    SMTP_USER: str = None
    SMTP_PASSWORD: str = None
    EMAILS_FROM_EMAIL: EmailStr = None
    EMAILS_FROM_NAME: str = None

    @validator("EMAILS_FROM_NAME")
    def get_project_name(cls, v, values):
        return values["PROJECT_NAME"]

    EMAIL_RESET_TOKEN_EXPIRE_HOURS: int = 48
    EMAIL_TEMPLATES_DIR: str = "/app/app/email-templates/build"
    EMAILS_ENABLED: bool = False

    @validator("EMAILS_ENABLED")
    def get_emails_enabled(cls, v, values):
        return (
            values["SMTP_HOST"] and values["SMTP_PORT"] and values["EMAILS_FROM_EMAIL"]
        )

    FIRST_SUPERUSER: EmailStr = None
    FIRST_SUPERUSER_PASSWORD: str

    EMAIL_TEST_USER: EmailStr = "test@test.com"

    USERS_OPEN_REGISTRATION: bool = False

    class Config:
        case_sensitive = True


# Initialize settings, which will pull from the environment if present.
settings = Settings()
