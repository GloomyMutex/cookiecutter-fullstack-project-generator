#! /usr/bin/env bash

# Run this script from outside the project, to integrate a dev-cfpg project with changes and review modifications

# Exit in case of error
set -e

if [ $(uname -s) = "Linux" ]; then
    echo "Remove __pycache__ files"
    sudo find ./dev-cfpg/ -type d -name __pycache__ -exec rm -r {} \+
fi

rm -rf ./cookiecutter-fullstack-project-generator/\{\{cookiecutter.project_slug\}\}/*

rsync -a --exclude=node_modules ./dev-cfpg/* ./cookiecutter-fullstack-project-generator/\{\{cookiecutter.project_slug\}\}/

rsync -a ./dev-cfpg/{.env,.gitignore,.gitlab-ci.yml} ./cookiecutter-fullstack-project-generator/\{\{cookiecutter.project_slug\}\}/
